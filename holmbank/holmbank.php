<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!defined('_PS_VERSION_'))
{
    exit;
}

if (!class_exists('HolmbankApi'))
{
    include(_PS_MODULE_DIR_.'holmbank/src/api/HolmbankApi.php');
}

if (!class_exists('MerchantProductRepository'))
{
    include(_PS_MODULE_DIR_.'holmbank/src/dal/MerchantProductRepository.php');
}


class Holmbank extends PaymentModule
{
    /**
     * Defines default payment type that should be automatically inserted in Database during install.
     */
    const DEFAULT_PAYMENT_TYPE = 'PRN_HP';

    /**
     * Module initializer.
     */
    public function __construct()
    {
        $this->name = 'holmbank';
        $this->tab = 'payments_gateways';
        $this->version = '1.1.0';
        $this->author = 'Veebipoed.ee';
        $this->bootstrap = true;
        $this->controllers = array('payment', 'validate', 'return');

        parent::__construct();

        $this->displayName = $this->l("Holmbank Payment");
        $this->description = $this->l('Module for making Holmbank payments');
        $this->frontModuleText = sprintf($this->l('Pay by %s'), $this->displayName);
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
        $this->is_eu_compatible = 1;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.99.99');
    }

    /**
     * Module install.
     * @return bool True, if successful and false if not.
     */
    public function install()
    {
        $sql_result = include(dirname(__FILE__) . '/sql/install.php');

        return parent::install() &&
            $sql_result &&
            $this->registerHook('payment') &&
            $this->registerHook('displayPaymentReturn') &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->registerHook('displayHeader') &&
            $this->registerHook('displayProductPriceBlock') &&
            $this->createOrderStates();
   }

    /**
     * Module uninstall.
     * @return bool True, if successful and false if not.
     */
    public function uninstall()
    {
        $sql_result = include(dirname(__FILE__) . '/sql/uninstall.php');

        return $sql_result &&
            $this->unregisterHook('paymentOptions') &&
            $this->unregisterHook('displayPaymentReturn') &&
            $this->unregisterHook('displayBackOfficeHeader') &&
            $this->unregisterHook('displayHeader') &&
            $this->unregisterHook('displayProductPriceBlock') &&
            parent::uninstall();
    }

    /**
     * Display and Process Module Configuration Page.
     * @return string Configuration Page HTML template.
     */
    public function getContent()
    {
        $html = '';
        $merchantProductRepository = new MerchantProductRepository();

        // User wants to modify already presented Merchant Loan Product.
        if (isset($_GET['updateholmbank__merchant_loan_products']) && isset($_GET['id_merchant_product']))
        {
            return $this->buildMerchantLoanForm($_GET['id_merchant_product']);
        }

        // User wants to create new Product.
        if (Tools::isSubmit('init_new_merchant_product_' . $this->name))
        {
            return $this->buildMerchantLoanForm();
        };

        // User wants to delete already presented Merchant Loan Product.
        if (isset($_GET['deleteholmbank__merchant_loan_products']) && isset($_GET['id_merchant_product']))
        {
            $merchantProductRepository->deleteMerchantProduct($_GET['id_merchant_product']);
            $html .= $this->displayConfirmation($this->l('Merchant product was successfully deleted'));
        }

        // User has filled form and wants to create Merchant Loan Product
        if (Tools::isSubmit('add_new_merchant_product_' . $this->name))
        {
            $merchantProductRepository->addMerchantProduct($_POST);
            $html .= $this->displayConfirmation($this->l('Merchant product was successfully created'));
        }

        // User has filled form and wants to modify Merchant Loan Product
        if (Tools::isSubmit('modify_merchant_product_' . $this->name))
        {
            $merchantProductRepository->updateMerchantProduct($_POST);
            $html .= $this->displayConfirmation($this->l('Merchant product was successfully modified'));
        }

        // User wants to save module settings.
        if (Tools::isSubmit('save_module_settings_' . $this->name))
        {
            foreach ($this->getParams() as $param)
            {
                Configuration::updateValue($this->prefixed($param), Tools::getValue($param));
            }

            $html .= $this->displayConfirmation($this->l('Holmbank settings were updated'));
        }

        $html .= $this->DisplayAddMerchantProduct();
        $html .= $this->buildMerchantLoanTable();
        $html .= $this->buildSettingsForm();

        return $html;
    }

    /**
     * Method builds form for new Merchant Loan Product creation.
     * @return mixed Generated form in HTML.
     */
    private function buildMerchantLoanForm($merchantLoanProductId = null)
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper = new HelperForm();

        $helper->module = $this;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->name_controller = $this->name;
        $helper->submit_action = 'submit'.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Form should be suitable for editing.
        if ($merchantLoanProductId != null)
        {
            $merchantProductRepository = new MerchantProductRepository();
            $merchantLoan = $merchantProductRepository->getMerchantProductById($merchantLoanProductId);
            $form_title = $this->l('Modify merchant product');
            $form_submit_button_name = 'modify_merchant_product_' . $this->name;
        }


        // Init form with values
        $form_fields = array(
            'type' => isset($merchantLoan['type']) ? $merchantLoan['type'] : '',
            'name' => isset($merchantLoan['name']) ? $merchantLoan['name'] : '',
            'description' => isset($merchantLoan['description']) ? $merchantLoan['description'] : '',
            'logo' => isset($merchantLoan['logo']) ? $merchantLoan['logo'] : '',
            'is_enabled' => isset($merchantLoan['is_enabled']) ? $merchantLoan['is_enabled'] : false,
            'is_calculator_displayed' => isset($merchantLoan['is_calculator_displayed']) ? $merchantLoan['is_calculator_displayed'] : false,
            'interest_rate' => isset($merchantLoan['interest_rate']) ? $merchantLoan['interest_rate'] : 0,
            'payment_period' => isset($merchantLoan['payment_period']) ? $merchantLoan['payment_period'] : 0,
            'calculator_text' => isset($merchantLoan['calculator_text']) ? $merchantLoan['calculator_text'] : '',
            'calculator_link' => isset($merchantLoan['calculator_link']) ? $merchantLoan['calculator_link'] : '',
        );

        if ($merchantLoanProductId != null)
        {
            $form_fields['id_merchant_product'] = $merchantLoanProductId;
        }

        // Load Data from API for Loan Product Type select field.
        $holmbankApi = new HolmbankApi();
        $loanProductTypes = $holmbankApi->getLoanProducts();

        $loanProductTypeOptions = array();

        foreach ($loanProductTypes as $loanProductType)
        {
            $loanProductTypeOption = [
                'id_option' => $loanProductType['type'],
                'name' => $loanProductType['name']
            ];

            $loanProductTypeOptions[] = $loanProductTypeOption;
        }

        $configurationForm[0]['form'] = array(
            'legend' => array(
                'title' => isset($form_title) ? $form_title : $this->l('Create new merchant product'),
                'icon'  => 'icon-cogs',
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('Type'),
                    'name' => 'type',
                    'required' => false,
                    'options' => array(
                        'query' => $loanProductTypeOptions,
                        'id' => 'id_option',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'text',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'required' => false,
                ),
                array(
                    'type' => 'textarea',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('Description'),
                    'name' => 'description',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('Logo'),
                    'name' => 'logo',
                    'required' => false,
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Active'),
                    'name' => 'is_enabled',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Calculator Display'),
                    'name' => 'is_calculator_displayed',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'is_calculator_displayed_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'is_calculator_displayed_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'text',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('Interest Rate'),
                    'name' => 'interest_rate',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('Payment Period'),
                    'name' => 'payment_period',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('Calculator Text'),
                    'name' => 'calculator_text',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('Calculator Link'),
                    'name' => 'calculator_link',
                    'required' => false,
                ),
                ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => isset($form_submit_button_name) ? $form_submit_button_name : 'add_new_merchant_product_' . $this->name
            ),
        );

        // In case of update add Product ID value.
        if ($merchantLoanProductId != null)
        {
            $configurationForm[0]['form']['input'][] = array (
                'type' => 'hidden',
                'rows' => 10,
                'cols' => 100,
                'name' => 'id_merchant_product',
            );
        }

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->languages = Language::getLanguages(false);

        foreach ($helper->languages as $k => $language)
        {
            $helper->languages[$k]['is_default'] = (int)($language['id_lang'] == $helper->default_form_language);
        }

        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                        '&token='.Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        $helper->fields_value = $form_fields;
        return $helper->generateForm($configurationForm);
    }

    /**
     * Method builds section with option to add Merchant Product.
     * @return mixed Generated section with button to add Merchant Product.
     */
    private function DisplayAddMerchantProduct()
    {
        return $this->display($this->getLocalPath().$this->name.'.php', 'add_merchant_product_section.tpl');
    }

    /**
     * Method builds merchant product table in back office.
     * @return mixed Generated table HTML.
     */
    private function buildMerchantLoanTable()
    {
        $merchantProductRepository = new MerchantProductRepository();
        $merchantLoans = $merchantProductRepository->getAllMerchantProducts();

        $fields_list = array(
            'id_merchant_product' => array(
                'title' => $this->l('Id'),
                'width' => 140,
                'type' => 'int',
            ),
            'type' => array(
                'title' => $this->l('Type'),
                'width' => 140,
                'type' => 'text',
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'width' => 140,
                'type' => 'text',
            ),
            'description' => array(
                'title' => $this->l('Description'),
                'width' => 140,
                'type' => 'text',
            ),
            'is_enabled' => array(
                'title' => $this->l('Active'),
                'width' => 140,
                'type' => 'bool',
                'active' => 'status',
            ),
            'is_calculator_displayed' => array(
                'title' => $this->l('Calculator Display'),
                'width' => 140,
                'type' => 'bool',
                'active' => 'status',
            ),
            'interest_rate' => array(
                'title' => $this->l('Interest rate'),
                'width' => 140,
                'type' => 'float',
            ),
            'payment_period' => array(
                'title' => $this->l('Payment period'),
                'width' => 140,
                'type' => 'float',
            ),
            'calculator_text' => array(
                'title' => $this->l('Calculator text'),
                'width' => 140,
                'type' => 'text'
            ),
            'calculator_link' => array(
                'title' => $this->l('Calculator link'),
                'width' => 140,
                'type' => 'text'
            ),
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_merchant_product';
        $helper->show_toolbar = true;
        $helper->actions = array('edit', 'delete');
        $helper->title = $this->l('Merchant loan products');
        $helper->table = $this->name.'__merchant_loan_products';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        return $helper->generateList($merchantLoans, $fields_list);
    }

    /**
     * Method builds Holmbank configuration block in Back Office.
     * @return mixed Generated form HTML.
     */
    private function buildSettingsForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $field_values = array();

        $configurationForm[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Holmbank basic configurations'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'rows' => 10,
                    'cols' => 100,
                    'label' => $this->l('API key'),
                    'name' => 'x-payment-link-key',
                    'required' => false,
                    'lang' => false
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Live Mode'),
                    'name' => 'is-live-mode',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'save_module_settings_' . $this->name
            ),
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $helper->languages = Language::getLanguages(false);

        foreach ($helper->languages as $k => $language)
        {
            $helper->languages[$k]['is_default'] = (int)($language['id_lang'] == $helper->default_form_language);
        }

        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                        '&token='.Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        foreach ($this->getParams() as $param)
        {
            $field_values[$param] = Configuration::get($this->prefixed($param));
        }

        $helper->fields_value = $field_values;
        return $helper->generateForm($configurationForm);
    }


    // Hooks


    /**
     * Hook to display active payment options in cart.
     * @return string Payment option view.
     */
    public function hookPayment()
    {
        // Get only active merchant loan products to display to customer.
        $merchantProductRepository = new MerchantProductRepository();
        $merchantLoans = $merchantProductRepository->getActiveMerchantProducts();

        // Assemble payment options view.
        $htmlPayments = '';
        foreach ($merchantLoans as $merchantDBLoan) {
            $logoUrl = ('' == $merchantDBLoan['logo'] ? Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/logo.png') : $merchantDBLoan['logo']);

            $smartyVars = [
                'callToActionText' => $merchantDBLoan['name'],
                'additionalInformation' => $merchantDBLoan['description'],
                'urlToController' => $this->context->link->getModuleLink($this->name, 'validate', array(
                    'loan_product_type' => $merchantDBLoan['type'])),
                'logo' => $logoUrl,
            ];

            $this->smarty->assign($smartyVars);
            $htmlPayments .= $this->display(__FILE__, 'views/templates/hook/payment.tpl');
        }
        return $htmlPayments;
    }

    /**
     * Method adds CSS and JS to the BO header.
     * @return void
     */
    public function hookDisplayBackOfficeHeader()
    {
        // Load Data from API for Loan Product Type select field.
        $holmbankApi = new HolmbankApi();
        $loanProductTypes = $holmbankApi->getLoanProducts();

        $this->context->controller->addJs(($this->_path."views/js/").'admin.js', 'all');
        Media::addJsDef(['loanProductTypes' => $loanProductTypes]);
    }

    /**
     * Hook for displaying elements on the product page.
     * @param $params
     * @return false|string Nothing if call doesn't meet our criteria otherwise HTML element.
     */
    public function hookDisplayProductPriceBlock($params) {

        // Element should be placed only after price block.
        if ($params['type'] != 'after_price') {
            return false;
        }

        if (Tools::isSubmit('id_product')) {
            // Get only active merchant loan products to display to customer.
            $merchantProductRepository = new MerchantProductRepository();
            $merchantLoans = $merchantProductRepository->getCalculatorMerchantProducts();

            $html_calculator = '';
            foreach ($merchantLoans as $merchantDBLoan)
            {
                // Check if interest rate and payment period are set at all.
                if (empty($merchantDBLoan['interest_rate']) || empty($merchantDBLoan['payment_period'])) {
                    continue;
                }

                // Check that monthly payment meets criteria for display. (Should be equal to 7 or more)
                $product = new Product($params['product']->id);
                $monthlyPayment = number_format(($product->getPriceStatic((int)Context::getContext()->cookie->id_currency, true, null, 2) * (1 + $merchantDBLoan['interest_rate'] / 100 * $merchantDBLoan['payment_period'] / 12)) / ($merchantDBLoan['payment_period']), 2);

                $this->context->smarty->assign([
                    'holmbank_payment_logo' => ('' == $merchantDBLoan['logo'] ? Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/logo.png') : $merchantDBLoan['logo']),
                    'holmbank_payment_logo_alt' => $this->l('Payment method logo'),
                    'holmbank_calculator_estimation' => $monthlyPayment,
                    'shop_currency_code' => $this->context->currency->iso_code,
                    'holmbank_calculator_description' => $merchantDBLoan['calculator_text'],
                    'holmbank_calculator_link' => $merchantDBLoan['calculator_link'],
                ]);
                $html_calculator .= $this->display(__FILE__, 'views/templates/hook/product_page_calculator.tpl');
            }

            return $html_calculator;
        }
    }

    /**
     * Method adds CSS and JS to the front header.
     * @return void
     */
    public function hookDisplayHeader()
    {
        $this->context->controller->addCss($this->_path . 'views/css/holmbank.css');
    }

    /**
     * Payment return.
     * @return void
     */
    public function hookPaymentReturn($params)
    {
        $status = Tools::getValue('orderStatus');

        $this->smarty->assign(
            [
                'status' => $status,
            ]
        );

        return $this->display(__FILE__, 'views/templates/hook/payment_return.tpl');
    }

    /**
     * Method creates Order States for Holmbank Payment Process.
     * @return bool Indicator of success.
     */
    private function createOrderStates()
    {
        $holmbankStates = array(
            array(
                'display' => 'Holmbank awaiting',
                'configParam' => 'HOLMBANK_OS_AWAITING',
            ),
        );

        foreach ($holmbankStates as $holmbankState)
        {
            if ((bool) Configuration::get($holmbankState['configParam']))
            {
                continue;
            }

            $orderState = new OrderState();
            $orderState->name = [];

            foreach (Language::getLanguages() as $language)
            {
                $orderState->name[$language['id_lang']] = $holmbankState['display'];
            }

            $orderState->send_email = 1;
            $orderState->invoice = 1;
            $orderState->color = "lightblue";
            $orderState->unremovable = 0;
            $orderState->logable = 0;
            $orderState->delivery = 0;
            $orderState->hidden = 0;
            $orderState->template = 'bankwire';

            $orderState->add();
            Configuration::updateValue($holmbankState['configParam'], (int) $orderState->id);
        }

        return true;
    }


    // Utility Methods Below.


    /**
     * Method generates Prefixed Keys.
     * @param $key string Key.
     * @return string Prefixed Key
     */
    public function prefixed($key)
    {
        return $this->name.'_'.$key;
    }

    /**
     * Method contains settings parameters.
     * @return string[] setting parameters names.
     */
    public function getParams()
    {
        return array('x-payment-link-key', 'is-live-mode');
    }
}