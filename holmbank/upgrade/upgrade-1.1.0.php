<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


function upgrade_module_1_1_0($module)
{
    $sql_queries[] = 'ALTER TABLE ' . _DB_PREFIX_ . 'holmbank_merchant_product ADD calculator_link VARCHAR(256)';
    $sql_queries[] = 'ALTER TABLE ' . _DB_PREFIX_ . 'holmbank_merchant_product ADD is_calculator_displayed TINYINT(1) NOT NULL DEFAULT 0';

    try {
        foreach ($sql_queries as $query)
        {
            if (!Db::getInstance()->execute($query))
            {
                return false;
            }
        }
    } catch (Exception $e) {
        return false;
    }

    return true;
}