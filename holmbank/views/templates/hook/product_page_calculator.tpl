{*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Holmbank
*  @copyright 2023 Veebipoed.ee, Holmbank
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

<div class="holmbank_container">
    <div class="holmbank_logo">
        <img src="{$holmbank_payment_logo}" alt="{$holmbank_payment_logo_alt}">
    </div>
    {if ($holmbank_calculator_estimation >= 7)}
        <div class="holmbank_content">
            <div class="holmbank_calculator_estimation">
                {if !empty($holmbank_calculator_link)}
                    <a href="{$holmbank_calculator_link}" target="_blank" class="estimation_text">{l s='Monthly payment from' mod='holmbank'}: {$holmbank_calculator_estimation} {$shop_currency_code}</a>
                {else}
                    <span class="estimation_text">{l s='Monthly payment from' mod='holmbank'}: {$holmbank_calculator_estimation} {$shop_currency_code}</span>
                {/if}
            </div>
            <div class="holmbank_calculator_description">
                {$holmbank_calculator_description}
            </div>
        </div>
    {/if}
</div>
