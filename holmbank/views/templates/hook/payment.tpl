{*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Holmbank
*  @copyright 2023 Veebipoed.ee, Holmbank
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

<p class="payment_module holmbank">
    <a href="{$urlToController|escape:'html'}" title="{$callToActionText}" class="holmbank-payment-link">
        <img src="{$logo}" alt="{$callToActionText}"/>
        {$callToActionText}
        {if $additionalInformation}
            <span>({$additionalInformation})</span>
        {/if}
    </a>
</p>