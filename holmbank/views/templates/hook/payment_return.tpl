{**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{if $status == 'APPROVED'}
    <p class="alert alert-success">
        {l s='Your order on %s is complete.' sprintf=[$shop_name] d='Modules.Wirepayment.Shop'}<br/>
    </p>
    <strong>{l s='Your order will be sent as soon as we receive payment.' d='Modules.Wirepayment.Shop'}</strong>
    <p>
        {l s='If you have questions, comments or concerns, please contact our expert customer support team.' d='Modules.Wirepayment.Shop'}
    </p>
{elseif $status == 'PENDING'}
    <p class="alert alert-success">
        {l s='Your order on %s is complete.' sprintf=[$shop_name] d='Modules.Wirepayment.Shop'}<br/>
    </p>
    <strong>{l s='Your order will be sent as soon as we receive payment.' d='Modules.Wirepayment.Shop'}</strong>
    <p>
        {l s='If you have questions, comments or concerns, please contact our expert customer support team.' d='Modules.Wirepayment.Shop'}
    </p>
{else}
    <p class="alert alert-danger">
        {l s='We noticed a problem with your order. If you think this is an error, feel free to contact our expert customer support team.' d='Modules.Wirepayment.Shop' }
    </p>
{/if}

