{*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
*
*  @author    Veebipoed.ee, Holmbank
*  @copyright 2023 Veebipoed.ee, Holmbank
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

<div class="alert alert-info">
    <form method="post">
        <p>
            <label>{l s='Click to add new merchant product' mod='holmbank'}</label>
            <input id="mysubmit_holmbank" name="init_new_merchant_product_holmbank" type="submit" value="Add" class="button" />
        </p>
    </form>
</div>