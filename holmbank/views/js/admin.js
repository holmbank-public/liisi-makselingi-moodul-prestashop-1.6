/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

// Insert loaded values from API.
document.addEventListener("DOMContentLoaded", function(event) {
    let paymentOption = $('#type').val();

    for (const [key, value] of Object.entries(loanProductTypes)) {
        if(value.type == paymentOption) {
            if ($('#name').val() == '') {
                $('#name').val(value.name);
            }
            if ($('#logo').val() == '') {
                $('#logo').val(value.logoUrl);
            }
        }
    }

    $('#type').on('change', function(e) {
        let paymentOption = $('#type').val();

        for (const [key, value] of Object.entries(loanProductTypes)) {
            if(value.type == paymentOption) {
                $('#name').val(value.name);
                $('#logo').val(value.logoUrl);
            }
        }
    })
});