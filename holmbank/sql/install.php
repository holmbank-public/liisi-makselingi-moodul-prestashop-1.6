<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$sql_queries = array();

$sql_queries[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'holmbank_merchant_product` (
    `id_merchant_product` INT(11) NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(256) NOT NULL,
    `name` VARCHAR(256),
    `description` VARCHAR(512),
    `logo` VARCHAR(256),
    `is_enabled` TINYINT(1) NOT NULL DEFAULT 0,
    `is_calculator_displayed` TINYINT(1) NOT NULL DEFAULT 0,
    `interest_rate` FLOAT,
    `payment_period` FLOAT,
    `calculator_text` VARCHAR(256),
    `calculator_link` VARCHAR(256),
    PRIMARY KEY  (`id_merchant_product`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql_queries[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'holmbank_orders` (
    `id_shop` INT(11) NOT NULL,
    `id_merchant_order` INT(11) NOT NULL,
    `id_holmbank_order` VARCHAR(256) NOT NULL,
    `x_payment_link_req_id` VARCHAR(256) NOT NULL,
    PRIMARY KEY  (`id_merchant_order`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

foreach ($sql_queries as $query)
{
    if (!Db::getInstance()->execute($query))
    {
        return false;
    }
}

return true;