<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!class_exists('HolmbankApi')) {
    include(_PS_MODULE_DIR_.'holmbank/src/api/HolmbankApi.php');
}

if (!class_exists('OrderRepository')) {
    include(_PS_MODULE_DIR_.'holmbank/src/dal/OrderRepository.php');
}


/**
 * Controller for Order Payment Validation.
 * Checks that data is suitable for beginning API requests.
 */
class HolmbankValidateModuleFrontController extends ModuleFrontController
{

    /**
     * Handle POST request.
     * @return void Redirect to Payment Controller.
     */
    public function postProcess()
    {
        $cart = $this->context->cart;

        // Check if order can be placed and module is turned on for payment process. Otherwise, back to the cart.
        if ($cart->id_customer == 0 ||
            $cart->id_address_delivery == 0 ||
            $cart->id_address_invoice == 0 ||
            !$this->module->active)
        {
            Tools::redirect('index.php?controller=order&step=1');
        }

        // Create new order.
        $this->module->validateOrder(
            $cart->id,
            Configuration::get('HOLMBANK_OS_AWAITING'),
            $cart->getOrderTotal(true, Cart::BOTH),
            'holmbank',
            null,
            [],
            null,
            false,
            $cart->secure_key
        );

        $idOrder = Order::getOrderByCartId($cart->id);

        // Assemble request body to send it to the API and Generate unique key.
        $requestBody = $this->assembleRequestBody($cart, $idOrder);

        // Cart total should always be equal to sum of products.
        $isValid = $this->orderTotalValidation($requestBody);

        // Totals are not equal to each other, so display error message to the customer and send him back to the cart page.
        if (!$isValid)
        {
            PrestaShopLogger::addLog('During checkout cart with ID ' . $cart->id . 'was unable to process Holmbank payment. 
                                                Reason: Cart total sum was not equal to separate products sum.', 1);
            $this->context->cart = new Cart($this->reassembleCart());
            Tools::redirect('index.php?controller=order&step=1');
        }

        // Control that loan product still exist in the backend.
        $isValid = $this->loanPaymentExistValidation($_REQUEST['loan_product_type']);

        // Product was deleted from API system, but still remains in Merchant System. Send customer back to cart.
        if (!$isValid)
        {
            PrestaShopLogger::addLog('During checkout cart with ID ' . $cart->id . 'was unable to process Holmbank payment. 
                                                Reason: Loan Type is not accessible any more in backend.', 1);
            $this->context->cart = new Cart($this->reassembleCart());
            Tools::redirect('index.php?controller=order&step=1');
        }

        // Order is valid and ready to process API requests.
        $uniqueKey = $this->generateUniqueKey($cart);

        $holmbankApi = new HolmbankApi();
        $response = $holmbankApi->postLoanStart($uniqueKey, $requestBody);
        $response = json_decode($response, true);

        // API returned data is not matched with expectations, so send customer back to the cart.
        if (empty($response["orderLink"]) || empty($response["orderId"]))
        {
            PrestaShopLogger::addLog('During checkout cart with ID ' . $cart->id . 'was unable to process Holmbank payment. 
                                                Reason: Holmbank API unexpected response.', 1);
            $this->context->cart = new Cart($this->reassembleCart());
            Tools::redirect('index.php?controller=order&step=1');
            return;
        }

        // Add Holmbank Order to the Database.
        $paymentData = [
            "id_shop" => (int)Context::getContext()->shop->id,
            "id_merchant_order" => $idOrder,
            "id_holmbank_order" => $response["orderId"],
            "x_payment_link_req_id" => $uniqueKey,
        ];

        $holmbankOrderRepository = new OrderRepository();
        $holmbankOrderRepository->addHolmbankOrder($paymentData);

        // Everything is fine. Redirect User to start payment.
        Tools::redirect($response["orderLink"]);
    }


    // Only helping methods below (calculations, validations etc.)


    /**
     * Method assemble request body to send it to the API.
     * - All cart products are added to request with final price after applying taxes.
     * - Cart rules are being added as separate products with negative values.
     * - Shipping is also being added as separate product.
     * @param $cart Cart Defines customer cart.
     * @return array Defines request body.
     */
    private function assembleRequestBody($cart, $idOrder)
    {
        // NB! orderNumber can not be sent, because it's not created yet.
        $requestBody = [
            "language" => $this->context->language->iso_code,
            "totalAmount" => $cart->getOrderTotal(),
            "paymentType" => $_REQUEST['loan_product_type'],
            "products" => array(),
            "returnUrl" => $this->context->link->getModuleLink(
                'holmbank',
                'return'
            ),
        ];

        // Assemble body with all chosen by client products.
        $products = $cart->getProducts();
        foreach ($products as $product)
        {
            $productInfo = [
                "productSKU" => $product['unique_id'],
                "productName" => $product['name'],
                "totalPrice" => $product['total_wt'],
                "quantity" => $product['quantity'],
            ];
            $requestBody["products"][] = $productInfo;
        }

        // Assemble body with all cart Discounts and Coupons.
        foreach ($cart->getCartRules() as $cartRule)
        {
            $productInfo = [
                "productName" => 'discount_' . $cartRule['code'],
                "totalPrice" => -$cartRule['value_real'],
                "quantity" => 1,
            ];
            $requestBody["products"][] = $productInfo;
        }

        // Assemble body with Shipping product.
        $requestBody["products"][] = [
            "productName" => "shipping",
            "totalPrice" => $cart->getPackageShippingCost(),
            "quantity" => 1,
        ];

        // Calculate total amount and add difference as product
        $productsTotal = 0;
        foreach ($requestBody['products'] as $product) {
            $productsTotal += $product['totalPrice'];
        }

        $totalDifference = $requestBody['totalAmount'] - $productsTotal;
        if ($totalDifference != 0) {
            $requestBody["products"][] = [
                "productName" => "totalRoundDifference",
                "totalPrice" => $totalDifference,
                "quantity" => 1,
            ];
        }

        return $requestBody;
    }

    /**
     * Method checks if order matches Holmbank API criteria.
     * - "Cart total amount should always be equal to the products total sum in request"
     * @param $requestBody array Defines request to be sent to API.
     * @return bool Defines order suitability to be sent to the API.
     */
    private function orderTotalValidation($requestBody)
    {
        $productCostSum = 0;
        $epsilon = 0.01;
        foreach ($requestBody['products'] as $product)
        {
            $productCostSum += $product['totalPrice'];
        }

        return abs($requestBody['totalAmount'] - $productCostSum) < $epsilon;
    }

    /**
     * Method checks that chosen loan product still exist in API system.
     * @param $loanProductType
     * @return bool
     */
    private function loanPaymentExistValidation($loanProductType)
    {
        $api = new HolmbankApi();
        $loanProducts = $api->getLoanProducts();

        foreach ($loanProducts as $loanProduct)
        {
            if ($loanProduct['type'] === $_REQUEST['loan_product_type'])
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Method generate unique key for payment transaction.
     * @return string unique value generated by e-store
     */
    private function generateUniqueKey($cart)
    {
        // Just to be sure, that values never be the same add cart id to the unique key.
        return bin2hex(random_bytes(40)) . $cart->id;
    }

    /**
     * Method assembles the cart based on the previous cart state.
     * @return int New cart ID value.
     * @return int
     */
    private function reassembleCart()
    {
        $old_cart_secure_key = $this->context->cart->secure_key;
        $old_cart_customer_id = (int) $this->context->cart->id_customer;

        $cart_products = $this->context->cart->getProducts();
        $this->context->cart = new Cart();
        $this->context->cart->id_lang = $this->context->language->id;
        $this->context->cart->id_currency = $this->context->currency->id;
        $this->context->cart->add();

        foreach ($cart_products as $product) {
            $this->context->cart->updateQty((int) $product['quantity'], (int) $product['id_product'], (int) $product['id_product_attribute']);
        }

        if ($this->context->cookie->id_guest) {
            $guest = new Guest($this->context->cookie->id_guest);
            $this->context->cart->mobile_theme = $guest->mobile_theme;
        }

        $this->context->cart->id_customer = $old_cart_customer_id;
        $this->context->cart->save();

        if ($this->context->cart->id) {
            $this->context->cookie->id_cart = (int) $this->context->cart->id;
            $this->context->cookie->write();
        }

        $id_cart = (int) $this->context->cart->id;
        $this->context->cart->secure_key = $old_cart_secure_key;

        return $id_cart;
    }
}