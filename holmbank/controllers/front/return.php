<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!class_exists('HolmbankApi'))
{
    include(_PS_MODULE_DIR_.'holmbank/src/api/HolmbankApi.php');
}

if (!class_exists('OrderRepository'))
{
    include(_PS_MODULE_DIR_.'holmbank/src/dal/OrderRepository.php');
}

/**
 * Controller for Order Payment return.
 * Handles User redirection to page based on order status.
 * Handles API POST request with results of User payment.
 */
class HolmbankReturnModuleFrontController extends ModuleFrontController
{

    /**
     * Process Payment Completion POST.
     * Handles API POST request with results of User payment.
     * @return void
     */
    public function postProcess()
    {
        // Handle only on POST requests.
        if ('POST' === $_SERVER['REQUEST_METHOD'])
        {
            // Get POST body
            $request = $this->getRequestBody();

            // POST comes without any data
            if (empty($request))
            {
                PrestaShopLogger::addLog('Holmbank sent POST request to confirm or reject payment without any data', 1);
            }

            // Get record from Database that Order exist in Database.
            $orderRepository = new OrderRepository();
            $holmbankOrder = $orderRepository->getHolmbankOrderByIdAndKey($request['orderId'], $_SERVER['HTTP_X_PAYMENT_LINK_REQ_ID']);

            // Order was found in Database.
            if ($holmbankOrder != null)
            {
                // We need to change Order status based on Bank request data.
                $status = $request['status'];
                $order = new Order((int)$holmbankOrder['id_merchant_order']);

                $this->changeOrderStatus($status, $order);
            }
            die();
        }
    }

    /**
     * Process Payment Completion GET.
     * Handles User redirection to page based on order status.
     * @return void
     */
    public function initContent()
    {
        parent::initContent();

        // Check if everything fine and payment was proceeded.
        if (!isset($_GET['orderId']))
        {
            PrestaShopLogger::addLog('Holmbank tries to redirect client back to the shop without information about order', 1);
            Tools::redirect('index.php');
        }

        $orderRepository = new OrderRepository();
        $holmbankOrder = $orderRepository->getHolmbankOrderById($_GET['orderId']);

        $order = new Order((int)$holmbankOrder['id_merchant_order']);
        $cart = new Cart($order->id_cart);
        $customer = new Customer($order->id_customer);

        // Payment was unsuccessful, so reassemble the cart for user to chose another payment option.
        if ($order->current_state == (int) Configuration::get('PS_OS_CANCELED'))
        {
            $this->context->cart = new Cart($this->reassembleCart($cart));
            Tools::redirect('index.php?controller=order&step=1');
        }

        Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$order->id.'&key='.$customer->secure_key);

    }


    // Only helping methods below (calculations, validations etc.)


    /**
     * Method gets POST request body.
     * @return array Defines POST request body as data.
     */
    private function getRequestBody()
    {
        $raw = file_get_contents('php://input');
        return $raw == null ? [] : json_decode($raw, true);
    }

    /**
     * Method handles order status changes.
     * @param $status string Defines status that come from API.
     * @param $order Order Defines order which status should be changed.
     * @return void Process status changes.
     */
    private function changeOrderStatus($status, $order)
    {
        switch ($status)
        {
            case "APPROVED":
                $newStateName = 'PS_OS_PAYMENT';
                break;
            case "PENDING":
                $newStateName = 'HOLMBANK_OS_AWAITING';
                break;
            case "REJECTED":
                $newStateName = 'PS_OS_CANCELED';
                break;
            default:
                return;
        }

        $newState = (int) Configuration::get($newStateName);

        // Change state only if not the same.
        if ($order->current_state != $newState)
        {
            $order->setCurrentState($newState);
        }
    }

    /**
     * Method assembles the cart based on the previous cart state.
     * @param $cart Cart Defines cart.
     * @return int New cart ID value.
     */
    private function reassembleCart($cart)
    {
        $old_cart_secure_key = $cart->secure_key;
        $old_cart_customer_id = (int) $cart->id_customer;

        $cart_products = $cart->getProducts();
        $this->context->cart = new Cart();
        $this->context->cart->id_lang = $this->context->language->id;
        $this->context->cart->id_currency = $this->context->currency->id;
        $this->context->cart->add();

        foreach ($cart_products as $product) {
            $this->context->cart->updateQty((int) $product['quantity'], (int) $product['id_product'], (int) $product['id_product_attribute']);
        }

        if ($this->context->cookie->id_guest) {
            $guest = new Guest($this->context->cookie->id_guest);
            $this->context->cart->mobile_theme = $guest->mobile_theme;
        }

        $this->context->cart->id_customer = $old_cart_customer_id;
        $this->context->cart->save();

        if ($this->context->cart->id) {
            $this->context->cookie->id_cart = (int) $this->context->cart->id;
            $this->context->cookie->write();
        }

        $id_cart = (int) $this->context->cart->id;
        $this->context->cart->secure_key = $old_cart_secure_key;

        return $id_cart;
    }
}
