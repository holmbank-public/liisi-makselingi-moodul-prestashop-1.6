<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!class_exists('HolmbankApi'))
{
    include(_PS_MODULE_DIR_.'holmbank/src/api/HolmbankApi.php');
}


/**
 * Repository to process Database operations with Merchant Product Table.
 */
class MerchantProductRepository
{

    /**
     * Method process insertion of merchant product in Database.
     * @param $merchantProduct array Merchant product data to be inserted in Database.
     * @return mixed boolean as indicator of successful operation.
     */
    public function addMerchantProduct($merchantProduct)
    {
        return Db::getInstance()->insert('holmbank_merchant_product', [
            'type' => pSQL($merchantProduct['type']),
            'name' => pSQL($merchantProduct['name']),
            'description' => pSQL($merchantProduct['description']),
            'logo' => pSQL($merchantProduct['logo']),
            'is_enabled' => (bool) $merchantProduct['is_enabled'],
            'is_calculator_displayed' => (bool) $merchantProduct['is_calculator_displayed'],
            'interest_rate' => (float) $merchantProduct['interest_rate'],
            'payment_period' => (float) $merchantProduct['payment_period'],
            'calculator_text' => pSQL($merchantProduct['calculator_text']),
            'calculator_link' => pSQL($merchantProduct['calculator_link']),
        ]);
    }

    /**
     * Method gets all merchant products from Database.
     * @return array Merchant products stored in database.
     */
    public function getAllMerchantProducts()
    {
        $query = "SELECT * FROM " . _DB_PREFIX_ . "holmbank_merchant_product";
        return Db::getInstance()->executeS($query);
    }

    /**
     * Method gets all merchant products from Database that are currently active.
     * @return array Merchant products stored in database.
     */
    public function getActiveMerchantProducts()
    {
        $query = "SELECT * FROM " . _DB_PREFIX_ . "holmbank_merchant_product WHERE is_enabled = true";
        return Db::getInstance()->executeS($query);
    }

    /**
     * Method gets all merchant products from Database that can be displayed on calculator and active.
     * @return array Merchant products stored in database.
     */
    public function getCalculatorMerchantProducts()
    {
        $query = "SELECT * FROM " . _DB_PREFIX_ . "holmbank_merchant_product WHERE is_enabled = true AND is_calculator_displayed";
        return Db::getInstance()->executeS($query);
    }

    /**
     * Method gets merchant product from Database based on ID.
     * @param $id int Merchant product ID.
     * @return array Merchant product.
     */
    public function getMerchantProductById($id)
    {
        $query = 'SELECT * FROM `%1$sholmbank_merchant_product` WHERE `id_merchant_product`="%2$s"';
        return Db::getInstance()->getRow(sprintf($query, _DB_PREFIX_, (int) $id));
    }

    /**
     * Method process update of merchant product in Database.
     * @param $merchantProduct array Merchant product data to be updated in Database.
     * @return mixed Indicator of successful operation.
     */
    public function updateMerchantProduct($merchantProduct)
    {
        return Db::getInstance()->update('holmbank_merchant_product', [
            'type' => pSQL($merchantProduct['type']),
            'name' => pSQL($merchantProduct['name']),
            'description' => pSQL($merchantProduct['description']),
            'logo' => pSQL($merchantProduct['logo']),
            'is_enabled' => (bool) $merchantProduct['is_enabled'],
            'is_calculator_displayed' => (bool) $merchantProduct['is_calculator_displayed'],
            'interest_rate' => (float) $merchantProduct['interest_rate'],
            'payment_period' => (float) $merchantProduct['payment_period'],
            'calculator_text' => pSQL($merchantProduct['calculator_text']),
            'calculator_link' => pSQL($merchantProduct['calculator_link']),
        ], 'id_merchant_product = ' . $merchantProduct['id_merchant_product']);
    }

    /**
     * Method process delete of merchant product from Database.
     * @param $id int Merchant product ID in Database.
     * @return bool Indicator of successful operation.
     */
    public function deleteMerchantProduct($id)
    {
        return Db::getInstance()->delete('holmbank_merchant_product', 'id_merchant_product = '.(int) $id);
    }
}